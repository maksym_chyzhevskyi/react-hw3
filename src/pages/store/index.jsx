import React from "react";
import { useOutletContext } from "react-router-dom";
import styles from "../../styles/store.module.scss";
import { Product } from "../../components/product";
import PropTypes from "prop-types";

export const Store = () => {
  const {
    products,
    favorites,
    handleFavoritesClick,
    productsInCart,
    handleCartClick,
    setModal,
    setModalData,
  } = useOutletContext();

  return (
    <div className={styles.Store}>
      <ul className={styles.StoreList}>
        {products.map((product) => (
          <Product
            key={product.id}
            product={product}
            favorites={favorites}
            handleFavoritesClick={handleFavoritesClick}
            productsInCart={productsInCart}
            handleCartClick={handleCartClick}
            setModal={setModal}
            setModalData={setModalData}
          ></Product>
        ))}
      </ul>
    </div>
  );
};

Store.propTypes = {
  product: PropTypes.shape({}).isRequired,
  favorites: PropTypes.array.isRequired,
  handleFavoritesClick: PropTypes.func.isRequired,
  productsInCart: PropTypes.array.isRequired,
  handleCartClick: PropTypes.func.isRequired,
  setModal: PropTypes.func.isRequired,
  setModalData: PropTypes.func.isRequired,
};
