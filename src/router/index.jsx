import { createBrowserRouter } from "react-router-dom";

import { App } from "../App";
import { Store } from "../pages/store";
import { Cart } from "../pages/cart";
import { Favorites } from "../pages/favorites";

export const router = createBrowserRouter([
  {
    element: <App />,
    path: "/",
    children: [
      {
        element: <Store />,
        index: true,
      },
      {
        element: <Cart />,
        path: "Cart",
      },
      {
        element: <Favorites />,
        path: "favorites",
      },
    ],
  },
]);
