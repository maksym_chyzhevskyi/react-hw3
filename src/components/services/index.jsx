export class Service {
  static async getAll() {
    const response = await fetch("/api/ajax.json");
    const data = await response.json();
    return data;
  }
}
