import React from "react";
import { Outlet } from "react-router-dom";
import styles from "../../styles/goodsSection.module.scss";

export const GoodsSection = ({
  isDataLoading,
  fetchError,
  products,
  favorites,
  handleFavoritesClick,
  productsInCart,
  handleCartClick,
  setModal,
  setModalData,
}) => {
  return (
    <div className={styles.GoodsSection}>
      {fetchError && <h2>Error: ${fetchError}</h2>}
      {
        <Outlet
          context={{
            products,
            favorites,
            handleFavoritesClick,
            productsInCart,
            handleCartClick,
            setModal,
            setModalData,
          }}
        />
      }
    </div>
  );
};
