import React from "react";
import styles from "../../styles/product.module.scss";
import PropTypes from "prop-types";
import { FavIco } from "../icons/favIco";
import { FavEmptyIco } from "../icons/favEmptyIco";
import { CartIco } from "../icons/cartIco";
import { CartEmptyIco } from "../icons/cartEmptyIco";

export const Product = ({
  product,
  favorites,
  handleFavoritesClick,
  productsInCart,
  handleCartClick,
  setModal,
  setModalData,
}) => {
  const { id, name, price, imgUrl, article, color } = product;
  const isFavorites = favorites.includes(id);
  const isCart = productsInCart.includes(id);
  const modalDataAddProduct = {
    header: "Do you want to add the item to the cart?",
    closeButton: true,
    name,
    price,
    action: [
      {
        text: "Ok",
        backgroundColor: "#1c8646",
        onClick: () => handleCartClick(id),
      },
      {
        text: "Cancel",
        backgroundColor: "#1c8646",
        onClick: null,
      },
    ],
  };

  const modalDataDeleteProduct = {
    header: "Do you want to remove the product from the cart?",
    closeButton: true,
    name,
    price,
    action: [
      {
        text: "Ok",
        backgroundColor: "#cc1934",
        onClick: () => handleCartClick(id),
      },
      {
        text: "Cancel",
        backgroundColor: "#cc1934",
        onClick: null,
      },
    ],
  };

  return (
    <>
      <div className={styles.ProductContainer}>
        <div className={styles.Product}>
          <div className={styles.body}>
            <div className={styles.img}>
              <img src={`./img/${imgUrl}`} alt="" />
            </div>
            <div className={styles.title}>{name}</div>
            <div className={styles.article}>
              <span className={styles.articleText}>Артикул</span>:&nbsp;
              <span className={styles.articleValue}>{article}</span>
            </div>
            <div className={styles.color}>
              <span className={styles.colorText}>Цвет</span>:&nbsp;
              <span className={styles.colorValue}>{color}</span>
            </div>
          </div>
          <div className={styles.bottom}>
            <div className={styles.actions}>
              <span className={styles.cart}>
                {isCart ? (
                  <span
                    onClick={() => {
                      setModalData(modalDataDeleteProduct);
                      setModal(true);
                    }}
                  >
                    <CartEmptyIco width={26} fill={"#cc1934"} />
                  </span>
                ) : (
                  <span
                    onClick={() => {
                      setModalData(modalDataAddProduct);
                      setModal(true);
                    }}
                  >
                    <CartIco width={26} fill={"#1c8646"} />
                  </span>
                )}
              </span>
              <span className={styles.favorites}>
                {isFavorites ? (
                  <span
                    onClick={() => {
                      handleFavoritesClick(id);
                    }}
                  >
                    <FavIco width={26} fill={"#ffda12"} />
                  </span>
                ) : (
                  <span
                    onClick={() => {
                      handleFavoritesClick(id);
                    }}
                  >
                    <FavEmptyIco width={26} fill={"#ffda12"} />
                  </span>
                )}
              </span>
            </div>
            <div className={styles.price}>
              <span className={styles.sum}>{price}</span>
              <span className={styles.currency}> usd</span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

Product.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string,
    price: PropTypes.number,
    imgUrl: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
  }).isRequired,
  favorites: PropTypes.array.isRequired,
  handleFavoritesClick: PropTypes.func.isRequired,
  productsInCart: PropTypes.array.isRequired,
  handleCartClick: PropTypes.func.isRequired,
  setModal: PropTypes.func.isRequired,
  setModalData: PropTypes.func.isRequired,
};

Product.defaultProps = {
  product: {
    name: "",
    price: 0,
    imgUrl: "",
    article: "",
    color: "",
  },
};
