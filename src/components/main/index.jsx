import React from "react";
import styles from "../../styles/main.module.scss";
import { GoodsSection } from "../goodsSection";

export const Main = ({
  isDataLoading,
  fetchError,
  products,
  favorites,
  handleFavoritesClick,
  productsInCart,
  handleCartClick,
  setModal,
  setModalData,
}) => {
  return (
    <div className={styles.Main__row}>
      <GoodsSection
        isDataLoading={isDataLoading}
        fetchError={fetchError}
        products={products}
        favorites={favorites}
        handleFavoritesClick={handleFavoritesClick}
        productsInCart={productsInCart}
        handleCartClick={handleCartClick}
        setModal={setModal}
        setModalData={setModalData}
      />
    </div>
  );
};
